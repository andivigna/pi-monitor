'use strict';
var config          = require('./config.js');
var fs              = require('fs');
var path            = require('path');
var express         = require("express");
var app             = express();
var server          = require('http').createServer(app);
var io              = require('socket.io')(server);
var bodyParser      = require('body-parser');
var cookieParser    = require('cookie-parser');
var request         = require('request');
var os              = require('os');
var osu              = require('os-utils');
var visitas = 0;
/*CONFIG & MIDDLEWARE*/
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname + '/views'));
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static(path.join(__dirname + '/public/')));
app.use(express.static(path.join(__dirname,"/bower_components/")));
app.use(cookieParser());

//ROUTES
app.use(function(req, res, next) {
    //Hacer algo en cada peticion
    next();
});
app.get('/', function(req, res){
    res.render('index',{ip: config.server.ip, port: config.server.port });
});
app.get('/api', function(req, res){
    //res.setHeader('Content-Type', 'application/json');
    res.status(200).json('{data:{"hola":"mundo"}}');
});
//Pagina no encontrada
app.use(function(req, res) {
    console.log("Pagina no encontrada.");
    var requestedUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
    var homeUrl =  req.protocol + '://' + req.get('host') + "/";
    console.log(requestedUrl + "\t" + homeUrl);
    res.status(404).render('notFound', { url: requestedUrl, home: homeUrl});
});

//SOCKET.IO
io.on('connection', function(socket){
    var cpuLoad;
    //console.log('Conexion: ID/IP', socket.id, '/', socket.request.connection.remoteAddress);
    socket.on('status',function(data){
        //Hacer algo
    });
    socket.on ('disconnect', function() {
        //Hacer algo cuando se cierra la conexion
    });
    setInterval(function(){
        osu.cpuUsage(function(v){
            cpuLoad = (v).toFixed(2);
        });
        let info = {
            os: {
                os: os.type() + ", " + os.platform(),
                arch: os.arch(),
                host: os.hostname(),
                uptime: (os.uptime() / 60 / 60 / 60).toFixed(2)  + " hs"

            },
            cpu: {
                model: os.cpus()[0].model,
                threads: os.cpus().length,
                speed: (os.cpus()[0].speed / 1000).toFixed(2)  + " Ghz",
                usage: os.loadavg()[0] + "% | " + os.loadavg()[1] + "% | " + os.loadavg()[2] + "%" ,
                process: []
            },
            ram: {
                total: (os.totalmem() / 1024 / 1024 / 1024).toFixed(3)  + " GB",
                free: (os.freemem() / 1024 / 1024 / 1024).toFixed(3)  + " GB",
                usage: (os.totalmem() / 1024 / 1024 / 1024 - os.freemem() / 1024 / 1024 / 1024).toFixed(3) + " GB"
            },
            hdd: {
                total: '',
                free: '',
                usage: ''
                
            },
        };

        let status = os.cpus()[0].model;
        //{cpu: 50, ram:20, hdd:1500}
        socket.emit('data', info);
    },1000);
});

server.listen(config.server.port, function(){
    console.log('Server running on %s:%d | PID %d', config.server.ip, config.server.port, config.server.pid);
    console.log("Sistema operativo: " + os.platform() + "\nArquitectura: " + os.arch() + "\nHost: " + os.hostname());
});
