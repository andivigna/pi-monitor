var argv = require('yargs').argv;
var ip = require('ip');

/*OBJECT TREE*/
var config = {};
config.server = {};
config.socket = {};

/*SERVER*/
config.server.ip = argv.ip || ip.address();
config.server.port = argv.port || 3000;
config.server.pid = process.pid;

/*SOCKET*/
config.socket.ip = argv.socketIp || ip.address;
config.socket.port = argv.socketPort || (argv.port || 3000);

module.exports = config;
