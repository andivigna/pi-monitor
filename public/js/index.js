$(document).ready(function() {
    /*ACCION BOTONES CLICK*/
    var btnActualizar = $("#btnCuentaAFicha");
    var lblStatus = $("#lblStatus");

    btnActualizar.click(function(){
        socket.emit('status');
    });

    socket.on('data', function(data){
        console.log(data);
        //lblStatus.empty();
        lblStatus.text( JSON.stringify( data ) );
        //$("#os").text( JSON.stringify( data.os ) );
        $("#sistema").empty();
        $("#sistema").append( "<p>OS: <strong>" + data.os.os +"</strong></p>" );
        $("#sistema").append( "<p>Arquitectura: <strong>" + data.os.arch +"</strong></p>" );
        $("#sistema").append( "<p>Host: <strong>" + data.os.host +"</strong></p>" );
        $("#sistema").append( "<p>Uptime: <strong>" + data.os.uptime +"</strong></p>" );

        $("#cpu").empty();
        $("#cpu").append( "<p>Modelo: <strong>" + data.cpu.model +"</strong></p>" );
        $("#cpu").append( "<p>Threads: <strong>" + data.cpu.threads +"</strong></p>" );
        $("#cpu").append( "<p>Frecuencia: <strong>" + data.cpu.speed +"</strong></p>" );
        $("#cpu").append( "<p>Uso: <strong>" + data.cpu.usage +"</strong></p>" );

        $("#ram").empty();
        $("#ram").append( "<p>Total: <strong>" + data.ram.total +"</strong></p>" );
        $("#ram").append( "<p>Libre: <strong>" + data.ram.free +"</strong></p>" );
        $("#ram").append( "<p>En uso: <strong>" + data.ram.usage +"</strong></p>" );

        $("#hdd").empty();
        $("#hdd").append( "<p>Total: <strong>" + data.hdd.total +"</strong></p>" );
        $("#hdd").append( "<p>Libre: <strong>" + data.hdd.free +"</strong></p>" );
        $("#hdd").append( "<p>En uso: <strong>" + data.hdd.usage +"</strong></p>" );
    });
});